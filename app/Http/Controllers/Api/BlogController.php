<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Models\Blog;
use App\Models\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $blogs = Blog::with('images')->get();

        return response()->json([
            'success' => true,
            'data' => $blogs,
        ], 200);
    }

    public function store(StoreBlogRequest $request)
    {
        $slug = Str::slug($request->title);
        $baseSlug = $slug;
        $counter = 1;

        while (Blog::where('slug', $slug)->exists()) {
            $slug = $baseSlug.'-'.$counter;
            $counter++;
        }

        $blogData = [
            'title' => $request->title,
            'slug' => $slug,
            'description' => $request->description,
            'user_id' => $request->user_id,
        ];

        DB::beginTransaction();

        $blog = Blog::create($blogData);

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $path = Storage::disk('public')->putFile('blogs', $image);
                Image::create(['blog_id' => $blog->id, 'image' => $path]);
            }
        }

        if ($blog) {

            DB::commit();

            $blog['images'] = $blog->images;

            return response()->json([
                'success' => true,
                'data' => $blog,
            ], 200);
        }

        DB::rollBack();

        return response()->json([
            'success' => false,
            'message' => 'Failed to store data',
        ], 500);

    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        if ($blog) {
            $blog['images'] = $blog->images;
            return response()->json([
                'success' => true,
                'data' => $blog,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'data' => null,
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        DB::beginTransaction();

        $slug = Str::slug($request->title);
        $baseSlug = $slug;
        $counter = 1;

        while (Blog::where('slug', $slug)->exists()) {
            $slug = $baseSlug.'('.$counter.')';
            $counter++;
        }
        $blogData = [
            'title' => $request->title,
            'slug' => $slug,
            'description' => $request->description,
        ];

        $blog->update($blogData);

        if ($request->hasFile('images')) {
            foreach ($blog->images as $image) {
                if (Storage::disk('public')->exists($image->image)) {
                    Storage::disk('public')->delete($image->image);
                }            
                $image->delete();
            }

            foreach ($request->file('images') as $image) {
                $path = Storage::putFile('blogs', $image);
                Image::create(['blog_id' => $blog->id, 'image' => $path]);
            }
        }
        if ($blog) {

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Updated successfully',
            ], 200);

        }

        DB::rollBack();

        return response()->json([
            'success' => false,
            'message' => 'Failed to update data.',
        ], 400);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        DB::beginTransaction();

        foreach ($blog->images as $image) {
            if (Storage::disk('public')->exists($image->image)) {
                Storage::disk('public')->delete($image->image);
            }            
            $image->delete();
        }

        $check = $blog->delete();

        if ($check) {
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Deleted successfully',
            ], 200);
        }

        DB::rollback();

        return response()->json([
            'success' => false,
            'message' => 'Failed to delete data',
        ], 400);
    }
}
